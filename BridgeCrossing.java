/*
 * Problem Statement (1000P):
 * A well-known riddle goes like this: Four people are crossing an old bridge.
 * The bridge cannot hold more than two people at once. It is dark, so they 
 * can't walk without a flashlight, and they only have one flashlight! 
 * Furthermore, the time needed to cross the bridge varies among the people 
 * in the group. For instance, let's say that the people take 1, 2, 5 and 10 
 * minutes to cross the bridge. When people walk together, they always walk 
 * at the speed of the slowest person. It is impossible to toss the flashlight
 * across the bridge, so one person always has to go back with the flashlight 
 * to the others. What is the minimum amount of time needed to get all the 
 * people across the bridge?
 * In this instance, the answer is 17. Person number 1 and 2 cross the bridge 
 * together, spending 2 minutes. Then person 1 goes back with the flashlight, 
 * spending an additional one minute. Then person 3 and 4 cross the bridge 
 * together, spending 10 minutes. Person 2 goes back with the flashlight 
 * (2 min), and person 1 and 2 cross the bridge together (2 min). This yields 
 * a total of 2+1+10+2+2 = 17 minutes spent.
 * You want to create a computer program to help you solve new instances of 
 * this problem. Given an int[] times, where the elements represent the time 
 * each person spends on a crossing, your program should return the minimum 
 * possible amount of time spent crossing the bridge.
 * Constraints
 * times will have between 1 and 6 elements, inclusive.
 * Each element of times will be between 1 and 100, inclusive.
 * Examples
 * 0) 
 * { 1, 2, 5, 10 }
 * Returns: 17
 * The example from the text.
 * 1) 
 * { 1, 2, 3, 4, 5 }
 * Returns: 16
 * One solution is: 1 and 2 cross together (2min), 1 goes back (1min), 4 and 5 
 * cross together (5min), 2 goes back (2min), 1 and 3 cross together (3min), 1 
 * goes back (1min), 1 and 2 cross together (2min). This yields a total of 2 + 1 
 * + 5 + 2 + 3 + 1 + 2 = 16 minutes spent.
 * 2)   
 * { 100 }
 * Returns: 100
 * Only one person crosses the bridge once.
 * 3) 
 * { 1, 2, 3, 50, 99, 100 }
 * Returns: 162
 */


//Solution: keep two linked list. left for the people on the left side of the 
//bridge (their current location) right for the right side(destination). 
//As mentioned in the problem statement in optimal solution two persons cross
//to the left each time and one person to the right. Then two solve problem
//keep the linked lists sorted. from left to right once select the two largest
//from the end of the list and once select the two smallest from the head of
//the list. From right to left every time select the smallest (head of the
//list). To implement the moving from left to right we use a turn. When the 
//turn is even we move from the end of the list when odd we move from the 
//head.
// running time:O(n) 
// memory: O(n)


import java.util.Arrays;
import java.util.LinkedList;

public class BridgeCrossing{
	private static LinkedList<Integer> left;
	private static LinkedList<Integer> right;
	
	public static int minTime(int[] times) {
		Arrays.sort(times);
		left = new LinkedList<>();
		right = new LinkedList<>();

		//Add the sorted element to left
		for (Integer i : times)
			left.add(i);

		//Start moving to the right two persons and moving 
		//to the left one person. This should be done until 
		//left becomes empty. timeSoFar keeps the time spent. 
		//We suppose that left has at least one element (based on the constriants of problem)
		int turn = 1;
		int timeSoFar = moveRight(turn++);
		while (!left.isEmpty()) {
			timeSoFar += moveLeft();
			timeSoFar += moveRight(turn++);
		}
		return timeSoFar;
	}

	//This method moves the smallest from right to left. Keep left sorted.
	public static int moveLeft() {
		int result = right.removeFirst();	
		if (result > left.getFirst()){
			left.add(1, result);		
		}else{
			left.addFirst(result);
		}
		return result;
	}

	//This method moves two persons from left to the right from head or tail
	//based on the turn. Keep right sorted.
	public static int moveRight(int turn) {
		if (turn % 2 == 0) {
			Integer a = left.removeLast();

			if (!left.isEmpty()){
				Integer b = left.removeLast();
				right.addLast(b);
			}
			
			right.addLast(a);
			return a;
		} else {
			Integer a = left.removeFirst();
			
			if (!left.isEmpty()){
				Integer b = left.removeFirst();
				right.addFirst(b);
				right.addFirst(a);
				return b;
			}else{
				right.addFirst(a);
				return a;
			}
		}
	}

}